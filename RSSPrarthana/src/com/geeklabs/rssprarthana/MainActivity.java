package com.geeklabs.rssprarthana;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geeklabs.rssprarthana.R;

public class MainActivity extends ActionBarActivity {

	private MediaPlayer mp = new MediaPlayer();
	private TextView prairText;
	private int currentPosition;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		prairText = (TextView) findViewById(R.id.prairText);

		try {
			AssetFileDescriptor descriptor = getAssets().openFd("Prarthana.mp3");
			mp.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();

			mp.prepare();
			mp.setLooping(true);
		} catch (Exception e) {
			e.printStackTrace();
		}

		findViewById(R.id.play_button).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				findViewById(R.id.play_button).setVisibility(View.INVISIBLE);
				findViewById(R.id.pauseButton).setVisibility(View.VISIBLE);
				mp.start();
			}
		});

		findViewById(R.id.pauseButton).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				findViewById(R.id.pauseButton).setVisibility(View.INVISIBLE);
				findViewById(R.id.play_button).setVisibility(View.VISIBLE);
				mp.pause();
			}
		});

		// Listen call states

		TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

		PhoneStateListener callStateListener = new PhoneStateListener() {
			public void onCallStateChanged(int state, String incomingNumber) {
				if (state == TelephonyManager.CALL_STATE_RINGING) {
					Toast.makeText(getApplicationContext(), "Phone Is Riging", Toast.LENGTH_LONG).show();
					mp.pause();
					currentPosition = mp.getCurrentPosition();
				}
				if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
					Toast.makeText(getApplicationContext(), "Phone is Currently in A call", Toast.LENGTH_LONG).show();
					mp.pause();
					currentPosition = mp.getCurrentPosition();
				}

				if (state == TelephonyManager.CALL_STATE_IDLE) {
					Toast.makeText(getApplicationContext(), "phone is neither ringing nor in a call", Toast.LENGTH_LONG).show();
					mp.pause();
					currentPosition = mp.getCurrentPosition();
				}
			}
		};

		telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.Telugu) {
			prairText.setText(getText(R.string.teluguPrair));
		}
		if (item.getItemId() == R.id.Hindi) {
			prairText.setText(getText(R.string.hindiPrair));
		}
		if (item.getItemId() == R.id.English) {
			prairText.setText(getText(R.string.englishPrair));
		}
		if (item.getItemId() == R.id.Malayalam) {
			prairText.setText(getText(R.string.malayalamPrayer));
		}
		if (item.getItemId() == R.id.Kannada) {
			prairText.setText(getText(R.string.kannadaPrayer));
		}
		/*
		 * if (item.getItemId() == R.id.Gujarati) {
		 * prairText.setText(getText(R.string.gujaratiPrayer)); } if
		 * (item.getItemId() == R.id.Gurumukhi) {
		 * prairText.setText(getText(R.string.gurumukhiPrayer)); }
		 */
		if (item.getItemId() == R.id.Bangla) {
			prairText.setText(getText(R.string.banglaPrayer));
		}
		if (item.getItemId() == R.id.Tamil) {
			prairText.setText(getText(R.string.tamilPrayer));
		}
		if (item.getItemId() == R.id.Meaning) {
			prairText.setText(getText(R.string.meaningText));

			// change back ground image
			RelativeLayout rLayout = (RelativeLayout) findViewById(R.id.mainActivity);
			Resources res = getResources(); // resource handle
			Drawable drawable = res.getDrawable(R.drawable.flag);

			rLayout.setBackground(drawable);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mp.seekTo(currentPosition);
		mp.start();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
	}
}
